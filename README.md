Requirements:

php >=5.5.9
postgres >=9.6.3

Installation:

composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console server:run