<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Enquiry
{
    /**
     * @Assert\Length(min="5", minMessage="Please clarify address")
     */
    protected $address;

    /**
     * @Assert\Range(min="1500", minMessage="The radius must be at least 1500 meters")
     */
    protected $radius;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param mixed $radius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
    }
}