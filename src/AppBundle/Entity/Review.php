<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * One Search has One Review.
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Search", inversedBy="review1")
     * @ORM\JoinColumn(name="search_id", referencedColumnName="id")
     */
    private $search;
    
    /**
     * @return mixed
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param Search $search
     */
    public function setSearch(Search $search)
    {
        $this->search = $search;
    }

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="search_id", type="integer", unique=false)
     */
    private $searchId;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="reviewed", type="text")
     * @Assert\Length(
     *     min=5,
     *     minMessage="Please leave more information"
     * )
     */
    private $reviewed;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set searchId
     *
     * @param integer $searchId
     *
     * @return Review
     */
    public function setSearchId($searchId)
    {
        $this->searchId = $searchId;

        return $this;
    }

    /**
     * Get searchId
     *
     * @return int
     */
    public function getSearchId()
    {
        return $this->searchId;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Review
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set reviewed
     *
     * @param string $reviewed
     *
     * @return Review
     */
    public function setReviewed($reviewed)
    {
        $this->reviewed = $reviewed;

        return $this;
    }

    /**
     * Get reviewed
     *
     * @return string
     */
    public function getReviewed()
    {
        return $this->reviewed;
    }
}

