<?php
// src/AppBundle/Controller/SearchControllerController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Review;
use AppBundle\Entity\Search;
use AppBundle\Form\Type\ReviewFormType;
use AppBundle\Form\Type\SearchFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;

class SearchControllerController extends Controller
{
    /**
     * @Route("/", name="search")
     */
    public function searchAction(Request $request)
    {
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getNormData();
            return $this->redirectToRoute('show_search_results', ['address' => $data->getAddress(), 'radius' => $data->getRadius()]);
        }elseif($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('warning', 'Please clarify your search request');
        }

        $formView = $form->createView();
        return $this->render('search/search.html.twig', compact('formView'));
    }

    /**
     * @Route("/search-results", name="show_search_results")
     */
    public function searchResultsPage(Request $request)
    {
        $address = $request->get('address');
        $radius = $request->get('radius') ;
        $placesFromApi = static::enquiryDataSearch($address, $radius);
        $repository = $this->getDoctrine()->getRepository(Search::class);
        $placesFromDb = $repository->findAll();
        $placeIdFromDb = array_column($placesFromDb, 'placeId');

        if(isset($placesFromApi->error_message)){
            return $this->render('search/search_results.html.twig', compact('placesFromApi'));
        }

        foreach ($placesFromApi->results as $place){
            if(in_array($place->place_id, $placeIdFromDb)){
                $place->isExistInDb = true;
            }else{
                $place->isExistInDb = false;
            }
        }

        $this->addFlash('info', 'Your search returned ' . count($placesFromApi->results) . ' results.');
        return $this->render('search/search_results.html.twig', compact('placesFromApi'));
    }

    /**
     * @Route("/save-search-results", name="save_search_results")
     * @Method("post")
     */
    public function saveSearchResults(Request $request)
    {
        $places = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();
        $batchSize = 20;
        $counter = 0;

        if(empty($places['searchResult'])){
            return $this->redirectToRoute('show_search_results_from_db');
        }
        
        foreach ($places['searchResult'] as $place) {
            $search = new Search();
            $search->setPlaceId($place['place_id']);
            $search->setAddress($place['address']);
            $search->setTitle($place['title']);
            $entityManager->persist($search);
            if (($counter % $batchSize) === 0) {
                $entityManager->flush();
                $entityManager->clear();
                $counter = 0;
            }
            $counter++;
        }
        $entityManager->flush();
        $entityManager->clear();

        return $this->redirectToRoute('show_search_results_from_db');
    }

    /**
     * @Route("/show-search-results-from-db", name="show_search_results_from_db")
     * @Method("post")
     */
    public function showResultFromDb()
    {
        $repository = $this->getDoctrine()->getRepository(Search::class);
        $placesFromDb = $repository->findAll();

        return $this->render('search/search_results_from_db.html.twig', compact('placesFromDb'));
    }

    /**
     * @Route("/review-place/{id}", name="review_place")
     * @Method("get")
     */
    public function reviewPlace(Search $place, Request $request)
    {
        $review = (!empty($place->getReview())) ? $place->getReview() : null;

        $form = $this->createForm(ReviewFormType::class, $review);
        $form->handleRequest($request);
        $id = $place->getId();

        $repository = $this->getDoctrine()->getRepository(Review::class);
        $review = $repository->findOneBy(['searchId' => $id]);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            if (!$review) {
                $review = new Review();
            }

            $data = $form->getNormData();

            $review->setSearchId($id);
            $review->setRating($data->getRating());
            $review->setReviewed($data->getReviewed());
            $review->setSearch($place);

            $entityManager->persist($review);
            $entityManager->flush();
            $entityManager->clear();
            $this->addFlash('success', 'Information successfully added to the database.');

        }elseif ($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('warning', 'Please fill in the form correctly.');
        }

        $formView = $form->createView();
        return $this->render('search/review_place.html.twig', compact('place', 'formView'));
    }

    /**
     * @Route("/delete-place/{id}", name="delete_place")
     * @Method("post")
     */
    public function deletePlace(Search $search)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
        $review = $search->getReview();
        if(!empty($review)){
            $entityManager->remove($review);
        }

        $entityManager->remove($search);
        $entityManager->flush();

        return $this->redirectToRoute('show_search_results_from_db');
    }

    /**
     * @param $address
     * @param $radius
     * @return mixed
     */
    static public function enquiryDataSearch($address, $radius)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/textsearch/json', [
            'query' => [
                'query' => $address,
                'radius' => $radius,
                'type' => 'restaurant',
                'key' => 'AIzaSyBVy2NGAeQbQVtDzufnk6HitceMnEyTeUk'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents());
    }

}
