<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{

    /**
     * @Route("/api/cafe", name="api_cafe")
     * @Method("get")
     */
    public function cafeAction()
    {
        $repository = $this->getDoctrine()->getRepository(Review::class);
        $reviewedPlaces = $repository->findAll();

        $data = (array)[];

        foreach ($reviewedPlaces as $place){
            $data[] = (object)[
                'id' => $place->getSearch()->getId(),
                'google_place_id' => $place->getSearch()->getPlaceId(),
                'title' => $place->getSearch()->getTitle(),
                'rating' => $place->getRating(),
                'review' => $place->getReviewed(),
            ];
        }

        return JsonResponse::fromJsonString(\GuzzleHttp\json_encode($data));
    }
    
}